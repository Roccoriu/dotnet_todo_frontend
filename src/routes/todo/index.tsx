import { For, Show, createSignal, onMount } from "solid-js";
import Card from "~/components/Card";
import Spinner from "~/components/Spinner";
import { deleteTodo, getTodos } from "~/services/todos";
import { BsPlus, BsTrash3 } from "solid-icons/bs";
import { useNavigate } from "solid-start";

export default function Home() {
  const navigate = useNavigate();
  const [data, setData] = createSignal({ loading: true, data: null });

  async function onFetch() {
    const res = await getTodos();
    setData({ loading: false, data: res });
  }

  onMount(async () => {
    await onFetch();
  });

  return (
    <>
      <button
        onClick={() => navigate("/todo/new")}
        class="flex px-5 py-2 hover:bg-[#00000010] focus:bg-[#00000020] items-center"
      >
        <BsPlus class="me-3" size={30} color="green" />
        <p>New Todo</p>
      </button>
      <hr class="w-[25rem] m-4 border-gray-300" />
      <Show when={!data().loading} fallback={<Spinner />}>
        <For each={data().data}>
          {({ id, title, description, isComplete, dueDate }) => (
            <Card
              title={
                <div class="grid grid-cols-9 gap-4">
                  <span class="col-span-8">{title}</span>
                  <button
                    onClick={async () => {
                      await deleteTodo(id);
                      await onFetch();
                    }}
                  >
                    <BsTrash3 color="red" />
                  </button>
                </div>
              }
              body={
                <span>
                  {description} {isComplete ? "👍" : "👎"}
                </span>
              }
            />
          )}
        </For>
      </Show>
    </>
  );
}
