import Card from "~/components/Card";
import CustomForm from "~/components/Form";
import { createTodo } from "~/services/todos";

export default function NewTodo() {
  return <Card body={<CustomForm onSubmit={createTodo} />} />;
}
