import { createSignal, onMount } from "solid-js";
import { getTodos } from "~/services/todos";
import { Outlet, useNavigate } from "solid-start";

export default function TodoLayout() {
  const navigate = useNavigate();
  const [data, setData] = createSignal({ loading: true, data: null });

  onMount(async () => {
    const res = await getTodos();
    setData({ loading: false, data: res });
  });

  return (
    <main
      style={{ height: "100%", overflow: "auto" }}
      class="  text-center  text-gray-700 p-4 flex flex-col justify-content-center items-center"
    >
      <Outlet />
    </main>
  );
}
