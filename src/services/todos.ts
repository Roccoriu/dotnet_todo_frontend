const baseUrl = import.meta.env.VITE_API_BASE_URL;

export async function getTodos() {
  const res = await fetch(`${baseUrl}/api/v1/todos`);

  if (res.ok) return res.json();

  console.log("Http Error", res.status);
}

export async function createTodo(data: any) {
  const res = await fetch(`${baseUrl}/api/v1/todos`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data),
  });

  if (res.ok) return res.json();
  console.log("Http Error", res.status);
}

export async function deleteTodo(id: number) {
  const res = await fetch(`${baseUrl}/api/v1/todos/${id}`, {
    method: "DELETE",
  });

  if (res.ok) return res.json();
  console.log("Http Error", res.status);
}
