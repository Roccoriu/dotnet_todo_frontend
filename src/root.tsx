// @refresh reload
import { Suspense } from "solid-js";
import {
  useLocation,
  A,
  Body,
  ErrorBoundary,
  FileRoutes,
  Head,
  Html,
  Meta,
  Routes,
  Scripts,
  Title,
  Outlet,
} from "solid-start";
import "./root.css";

export default function Root() {
  const location = useLocation();
  const active = (path: string) =>
    path == location.pathname
      ? "border-sky-600"
      : "border-transparent hover:border-sky-600";
  return (
    <Html lang="en">
      <Head>
        <Title>SolidStart - With TailwindCSS</Title>
        <Meta charset="utf-8" />
        <Meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <Body style={{ height: "100vh" }}>
        <Suspense>
          <ErrorBoundary>
            <nav class="bg-gray-900">
              <ul class="container flex items-center p-4 text-gray-200">
                <li class={`border-b-2 ${active("/todo")} mx-1.5 sm:mx-6`}>
                  <A href="/todo">Home</A>
                </li>
              </ul>
            </nav>
            <div
              style={{ height: "calc(100vh - 3.6rem)", overflow: "hidden" }}
              class="bg-gray-100"
            >
              <Routes>
                <FileRoutes />
              </Routes>
            </div>
          </ErrorBoundary>
        </Suspense>
        <Scripts />
      </Body>
    </Html>
  );
}
