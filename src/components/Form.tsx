import { Show, createSignal } from "solid-js";
import Input from "./Input";
import { useNavigate } from "solid-start";

interface TaskFormProps {
  onSubmit: (data: any) => Promise<void>;
  data?: {
    title?: string;
    description?: string;
    isComplete?: boolean | undefined;
    dueDate?: string;
  };
}

function TaskForm({ data = {}, onSubmit }: TaskFormProps) {
  const [title, setTitle] = createSignal(data.title || "");
  const [description, setDescription] = createSignal(data.description || "");
  const [isComplete, setIsComplete] = createSignal(
    data.isComplete || undefined
  );
  const [dueDate, setDueDate] = createSignal(
    data.dueDate || new Date().toISOString().slice(0, 10)
  );

  const navigate = useNavigate();

  const handleSubmit = async (e: Event) => {
    e.preventDefault();

    const formData = {
      title: title(),
      description: description(),
      isComplete: isComplete(),
      dueDate: dueDate(),
    };

    await onSubmit(formData);

    navigate("/todo");
  };

  return (
    <form onSubmit={handleSubmit} class="space-y-4">
      <label class="block">
        <Input
          type="text"
          label="Title"
          value={title()}
          onInput={(e) => setTitle((e.target as HTMLInputElement).value)}
        />
      </label>

      <label class="block">
        <Input
          type="text"
          label="Description"
          value={description()}
          onInput={(e) => setDescription((e.target as HTMLInputElement).value)}
        />
      </label>

      <Show when={isComplete()}>
        <label class="block">
          <input
            type="checkbox"
            checked={isComplete()}
            onChange={() => setIsComplete(!isComplete() ?? false)}
            class="mt-1 block rounded-md border-gray-300 shadow-sm"
          />
        </label>
      </Show>

      <label class="block">
        <Input
          type="date"
          label="Due Date"
          value={dueDate()}
          onInput={(e) => setDueDate((e.target as HTMLInputElement).value)}
        />
      </label>

      <button
        type="submit"
        class="py-2 px-4 bg-blue-600 text-white rounded-md hover:bg-blue-500 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50"
      >
        Submit
      </button>
    </form>
  );
}

export default TaskForm;
