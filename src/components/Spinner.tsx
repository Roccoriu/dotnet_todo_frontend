export default function Spinner({
  height = "3rem",
  width = "3rem",
}: {
  height?: string;
  width?: string;
}) {
  return (
    <div
      style={{ height, width }}
      class="inline-block  animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]"
      role="status"
    >
      <span class="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">
        Loading...
      </span>
    </div>
  );
}
