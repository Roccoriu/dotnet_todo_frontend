import { JSX, Show, createSignal } from "solid-js";

export interface CardProps {
  title?: string | JSX.Element;
  body?: JSX.Element;
  footer?: JSX.Element;
}

export default function Card({ title, body, footer }: CardProps) {
  return (
    <div class="bg-[#fff] drop-shadow-md border rounded-lg w-[25rem] m-4">
      <Show when={title}>
        <div class="px-5 py-3">{title}</div>
      </Show>
      <Show when={body}>
        <div class="border-t px-5 py-5">{body}</div>
      </Show>
      <div>{footer}</div>
    </div>
  );
}
